let student=[];



function addStudent(addStudent){
	student.push(addStudent);
	console.log(addStudent + " was added to the student's list.");
}

function countStudent(){
	console.log("There are a total of "+
		student.length+" students enrolled.");
}



function findStudent(findStudent) {

	let resultStudent = [];

	for(let i =0; i < student.length; i++) {
		if(student[i] == findStudent){
			resultStudent.push(student[i]);
		}}
	if (resultStudent.length == 1){
		console.log(resultStudent[0] + " is an enrollee.");
	}

	if (resultStudent.length > 1){
		console.log(resultStudent.join(',')+" are enrollees.");
	}

	if (resultStudent.length == 0){
		console.log("No student found with the name "+findStudent);
	}

}

function printStudent(){
	student.sort();
	student.forEach(function(list){
	console.log(list);
}); }

function removeStudent(removeStudent) {


	for(let i =0; i < student.length; i++) {

	if (student[i] == removeStudent){
		delete student[i];
		console.log(removeStudent+" was removed from the student's list.")
		break;
	}

} }